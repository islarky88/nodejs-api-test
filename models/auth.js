let mongoose = require("mongoose");
let timestamps = require('mongoose-timestamp');

let authSchema = mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: true
  },
  email: {
    type: String,
    trim: true,
    required: true
  },
  email_verified_at: {
    type: String,
    trim: true
  },
  password: {
    type: String,
    trim: true,
    required: true
  },
  api_token: {
    type: String,
    trim: true
  },
  remember_token: {
    type: String,
    trim: true
  }
});


authSchema.plugin(timestamps,  {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});

let Auth = module.exports = mongoose.model('auth', authSchema);
