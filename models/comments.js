let mongoose = require("mongoose");
let timestamps = require('mongoose-timestamp');

let commentSchema = mongoose.Schema({
  title: {
    type: String,
  },
  body: {
    type: String,
    trim: true,
    required: true

  },
  commentable_type: {
    type: String,
  },
  commentable_id: {
    type: Number,
  },
  creator_type: {
    type: String,
  },
  creator_id: {
    type: Number,
  },
  _lft: {
    type: Number,
  },
  _lgt: {
    type: Number,
  },
  parent_id: {
    type: Number,
  }
});





commentSchema.plugin(timestamps,  {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});

let Comments = module.exports = mongoose.model('comments', commentSchema);
