let mongoose = require("mongoose");
let timestamps = require('mongoose-timestamp');

let postSchema = mongoose.Schema({
  user_id: {
    type: Number,
    default: 1
  },
  title: {
    type: String,
    trim: true,
    required: true

  },
  slug: {
    type: String,
    required: true,
    lowercase: true,
    trim: true,
    unique: true
  },
  content: {
    type: String,
    required: true
  },
  image: {
    type: String
  },
  deleted_at: {
    type : Date,
    default: null
  }
});



postSchema.plugin(timestamps,  {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});



let Posts = module.exports = mongoose.model('posts', postSchema);
