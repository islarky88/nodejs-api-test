// Dependencies
const express = require("express");
const jwt = require("jsonwebtoken");
const path = require("path");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const expressValidator = require("express-validator");
const flash = require("connect-flash");
const session = require("express-session");
const cookieParser = require("cookie-parser");
var timestamps = require('mongoose-timestamp');
var slugify = require('slugify');

// Controllers
var PostController = require('./controllers/posts.js');
var CommentController = require('./controllers/comments.js');
var AuthController = require('./controllers/auth.js');


// init app
const app = express();
app.set('port', (process.env.PORT || 3000));
var sessionStore = new session.MemoryStore;


// Models
let Posts = require("./models/posts.js");
let Comments = require("./models/comments.js");
let Auth = require("./models/auth.js");

mongoose.connect("mongodb://localhost/nodekb", { useNewUrlParser: true });
let db = mongoose.connection;

db.once("open", function() {
  console.log("connected to mongo db");
}); 

//
db.on("error", function(err) {
  console.log(err);
});


// Load Pug View Engine
app.set("views", path.join(__dirname, "views"));
app.set("views engine", "pug");

// Set Public Folder
app.use(express.static(path.join(__dirname, "public")));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());


app.use(cookieParser('secret'));
app.use(session({
    cookie: { maxAge: 60000 },
    store: sessionStore,
    saveUninitialized: true,
    resave: 'true',
    secret: 'secret'
}));
app.use(flash());

// Custom flash middleware -- from Ethan Brown's book, 'Web Development with Node & Express'
app.use(function(req, res, next){
  // if there's a flash message in the session request, make it available in the response, then delete it
  res.locals.sessionFlash = req.session.sessionFlash;
  delete req.session.sessionFlash;
  next();
});





// +++++++++++++++++++++++++++++++++++
// +++++++++++++ ROUTES ++++++++++++++
// +++++++++++++++++++++++++++++++++++

app.get('/', function (req,res) {
  res.send('asd');
});

app.get('/api/posts/:postid/comments', CommentController.index);
app.post('/api/posts/:postid/comments', verifyToken, CommentController.create);
app.patch('/api/posts/:postid/comments/:commentid', verifyToken, CommentController.update);
app.delete('/api/posts/:postid/comments/:commentid', verifyToken, CommentController.delete);

app.get('/api/posts/:postid', PostController.show);
app.get('/api/posts', PostController.index);
app.post('/api/posts', verifyToken, PostController.create); //done
app.patch('/api/posts/:postid', verifyToken, PostController.update); //slightly done
app.delete('/api/posts/:postid', verifyToken, PostController.delete); //done


app.post('/api/register', verifyToken, AuthController.register);
app.post('/api/login', AuthController.login);
app.post('/api/logout', verifyToken, AuthController.logout);


// app.get("/", function(req, res) {
//   Posts.find({}, function(err, posts) {
//     if (err) {
//       console.log(err);
//     } else {
//       res.render("index.pug", {
//         title: "Articles",
//         expressFlash: req.flash('success'), 
//         sessionFlash: res.locals.sessionFlash,
//         posts: posts
//       });
//     }
//   });
// });

// app.get("/post/add", function(req, res) {
//   res.render("postAdd.pug", {
//     title: "Add Articles"
//   });
// });

// app.post("/posts", function(req, res) {
//   let posts = new Posts();
//   posts.title = req.body.title;
//   posts.content = req.body.content;
//   posts.image = req.body.image;

//   posts.save(function(err) {
//     if (err) {
//       console.log(err);
//       return;
//     } else {
//       //req.flash('success', 'This is a flash message using the express-flash module.');
//       res.redirect('/');
//     }
//   });
// });

// app.get("/api/post/:id", function(req, res) {
//   Posts.findById(req.params.id, function(err, posts) {
//     if (err) {
//       console.log(err);
//     } else {
//       res.render("postShow.pug", {
//         posts: posts
//       });
//     }
//   });
// });


// app.get("/post/:id", function(req, res) {
//   Posts.findById(req.params.id, function(err, posts) {
//     if (err) {
//       console.log(err);
//     } else {
//       res.render("postShow.pug", {
//         posts: posts
//       });
//     }
//   });
// });

// app.get("/post/edit/:id", function(req, res) {
//   // res.render("postEdit.pug", {
//   //   title: "Edit Post"
//   // });

//   Posts.findById(req.params.id, function(err, posts) {
//     if (err) {
//       console.log(err);
//     } else {
//       res.render("postEdit.pug", {
//         title: "Post Edit",
//         posts: posts
//       });
//     }
//   });
// });



// // Comments
// Route::get('posts/{post_id}/comments', 'CommentController@index');
// Route::post('posts/{post_id}/comments', 'CommentController@store');
// Route::patch('posts/{post_id}/comments/{comment_id}', ['as' => 'request', 'uses' => 'CommentController@update']);
// Route::delete('posts/{post_id}/comments/{comment_id}', ['as' => 'request', 'uses' => 'CommentController@destroy']);


// // Posts
// Route::get('posts/{post_id}', 'PostController@show');
// Route::get('posts', 'PostController@index');
// Route::post('posts', 'PostController@store');
// Route::patch('posts/{post_id}', 'PostController@update');
// Route::delete('posts/{post_id}', 'PostController@destroy');



// Route::post('register', 'Auth\RegisterController@register');
// Route::post('login', 'Auth\LoginController@login');
// Route::post('logout', 'Auth\LoginController@logout');

// GET request to delete Book.

// app.post("/api/posts", verifyToken, function (req, res) {
//   jwt.verify(req.token, "secretkey", function (err, data) {
//     if (err) {
//       res.sendStatus(403);
//     } 

//     let posts = new Posts();
//     posts.title = req.body.title;
//     posts.content = req.body.content;
//     posts.image = req.body.image;
//     posts.slug = slugify(req.body.title);

//     posts.save(function(err) {
//       if (err) {
//         console.log(err);
//         return;
//       } else {
//         //req.flash('success', 'This is a flash message using the express-flash module.');
//         res.redirect('/');
//       }
//     });

//   });
// });

// app.post("/api/login", function (req, res) {
//   const user = {
//     id: 1,
//     username: "brad",
//     email: "islarky88@gmail.com"
//   };
//   jwt.sign({ user }, "secretkey", { expiresIn: "99999999999s" }, (err, token) => {
//     res.json({
//       token
//     });
//   });
// });

//format of token

function verifyToken(req, res, next) {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    const bearer = bearerHeader.split(" ");
    const bearerToken = bearer[1];

    req.token = bearerToken;

    next();
  } else {
    res.sendStatus(403);
  }
}

app.listen(app.get('port'), function() {
  console.log("server started on port 8000");
});
