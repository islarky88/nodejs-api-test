$(document).ready(function() {
  $(".delete-article").on("click", function(e) {
    $target = $(e.target);
    const pid = $target.attr("data-id");
    $.ajax({
      type: "DELETE",
      url: "/post/edit",
      data: { "id": pid },
      success: function(response) {
        alert('Deletting Article with ID:' + pid);
        window.location.href = "/";
      },
      error: function (err) {
          console.log(err);
      }
    });
  });
});
