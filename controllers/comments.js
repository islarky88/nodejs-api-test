var Comments = require("../models/comments.js");
var slugify = require("slugify");
const jwt = require("jsonwebtoken");



module.exports = {
    index: function(req, res){
        res.send('All todos')
    },
    show: function(req, res){
        console.log('Viewing ' + req.params.id);
    },
    create: function(req, res){
        console.log('Todo created')
    },
    update: function(req, res){
        console.log('Todo deleted')
    },
    delete: function(req, res){
        console.log('Todo ' + req.params.id + ' updated')
    }
};