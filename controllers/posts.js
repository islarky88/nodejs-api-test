var Posts = require("../models/posts.js");
var slugify = require("slugify");
const jwt = require("jsonwebtoken");

exports.index = function(req, res) {
  res.send("All todos");
};
exports.show = function(req, res) {

      Posts.findOne({slug: req.params.postid}, function(err, posts) {
        if (err) {
          res.json({ err });
        } else {
            posts['id'] = 1;
            res.send({ data: posts });
        }
    });

};
exports.create = function(req, res) {
  jwt.verify(req.token, "secretkey", function(err, data) {
    if (err) {
      res.json({ err });
    }

    let posts = new Posts();
    posts.title = req.body.title;
    posts.content = req.body.content;
    posts.image = req.body.image;
    posts.slug = slugify(req.body.title);

    posts.save(function() {
      //req.flash('success', 'This is a flash message using the express-flash module.');
      res.json({ posts });
    });
  });
};
exports.update = function(req, res) {
  jwt.verify(req.token, "secretkey", function(err, data) {
    if (err) {
      res.json({ err });
    }

    let posts = {};

    posts.title = req.body.title;
    posts.content = req.body.content;
    posts.image = req.body.image;
    posts.slug = slugify(req.body.title);

    let query = { slug: req.params.postid };

    Posts.update(query, posts, function(err) {
      if (err) {
        res.json({ err });
      } else {
        res.json({ posts });
      }
    });
  });
};
exports.delete = function(req, res) {
  let query = { _id: req.params.postid };

  Posts.remove(query, function(err) {
    if (err) {
      res.send(err);
    } else {
      res.send("Success");
    }
  });
};

//   exports.update = function(req, res) {
//       //DONE
//       let query = { _id: req.params.id };

//       Posts.remove(query, function(err) {
//         if (err) {
//           res.send(err);
//         } else {
//           res.send("Success");
//         }
//       });
//     };

// module.exports = {
//     index: function(req, res){
//         res.send('All todos')
//     },
//     show: function(req, res){
//         console.log('Viewing ' + req.params.id);
//     },
//     store: function(req, res){
//         console.log('Todo created')
//     },
//     update: function(req, res){
//         console.log('Todo deleted')
//     },
//     destroy: function(req, res){
//         console.log('Todo ' + req.params.id + ' updated')
//     }
// };

// exports.show = function(req, res) {
//     //DONE
//     let query = { _id: req.params.id };

//     Posts.remove(query, function(err) {
//       if (err) {
//         res.send(err);
//       } else {
//         res.send("Success");
//       }
//     });
//   };

// exports.index = function(req, res) {
//     //DONE
//     let query = { _id: req.params.id };

//     Posts.remove(query, function(err) {
//       if (err) {
//         res.send(err);
//       } else {
//         res.send("Success");
//       }
//     });
//   };
